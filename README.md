Role Name
=========

This role will setup an HA cluster with at least two nodes or more

Requirements
------------
At least a minimum of two nodes

Role Variables
--------------
You will need to edit the `defaults/main` to list out what services you would like to the cluster to manage:

Example:

```
hacluster_pcs_monitor:
    - name: "zabbix_server"
      value: "zabbix-server"
    - name: "httpd"
      value: "httpd"
```

Dependencies
------------
None

Example Playbook
----------------
```
- hosts: zabbix
  roles:
    - { role: ansible_role_ha }
```

License
-------

MIT

Author Information
------------------

The Development Range Engineering, Architecture, and Modernization (DREAM) Team
